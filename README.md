# CFDInfo

**(PROYECTO MOVIDO A [GITHUB](https://github.com/dnlsrl/cfdinfo))**

CFDInfo es una utilería de línea de comandos para exportar los datos de comprobantes fiscales digitales (CFDI) XML a una hoja de cálculo (MS Excel) para una visualización más legible. Puede extraer la información desde un solo archivo hasta una cantidad ilimitada de ubicaciones de forma intuitiva.

### Requisitos

Para usar CFDInfo, es necesario tener el siguiente software instalado:

* Python 3.6.5 o mayor, con las siguientes dependencias:
	- lxml
	- openpyxl
	- suds-py3
* MS Excel o derivado (como Libreoffice u Openoffice, siempre y cuando sea compatible con la extensión de archivo ".xlsx")

### Uso y especificaciones generales

Para usar CFDInfo, no es importante ser experto en el uso de una consola de comandos, pero se debe tener una noción de cómo ejecutar un comando para poder usarlo correctamente.

La sintaxis es la siguiente:

`$ cfdinfo ruta [-v] [-p]`

En donde `cfdinfo` es el nombre de nuestro programa a ejecutar, `ruta` es el destino a un archivo, directorio o directorios, que puede indicar de forma indefinida, y `-v` y `-p` argumentos opcionales tanto para validar la vigencia del comprobante fiscal como para mostrar la información en pantalla en lugar de escribirla a un archivo.

Por el momento, estos son los datos que CFDInfo identifica:

* Serie
* Folio
* Fecha
* Subtotal
* Moneda
* Total
* Tipo de comprobante
* RFC y nombre del emisor
* RFC y nombre del receptor
* Impuestos individuales y total
* UUID
* Vigencia del comprobante (opcional)

**_Múltiples directorios_**

Cuando se especifican uno o varios directorios con CFDInfo, este inteligentemente busca en directorios internos también, lo cual es conveniente si usted tiene sus archivos CFDI XML guardados de forma ordenada. Por ejemplo, si en Documentos usted tiene un directorio llamado "Facturas" y dentro de ese tiene un directorio por cada mes en donde usted guarda los comprobantes fiscales por fecha, entonces CFDInfo incluirá todos esos archivos si especifica el siguiente comando:

`$ cfdinfo C:\Users\Usuario\Documents\Facturas\`

Como puede notar, en este caso, no será necesario indicar directorio por directorio sino únicamente el directorio raíz y CFDInfo intuitivamente buscará archivos con extensión XML dentro de cada directorio interno.

**_Visualización en pantalla_**

Si no considera necesario la creación de una hoja de cálculo para su consulta debido a la poca cantidad de archivos que desea revisar, el argumento `-p` o `--pantalla` mostrará los datos en la misma consola de comandos. Aun así, CFDInfo le dará la opción de guardar la información generada en un archivo.

**_Validación de vigencia_**

CFDInfo también es capaz de consultar la vigencia de un comprobante fiscal. Para esto, deberá activar el argumento `-v` o `--validar`. Es importante que se asegure que tiene una conexión a internet activa, de otra forma aparecerá el mensaje "Error" en la última columna de la hoja de cálculo generada. También tenga en cuenta que este proceso llevará tiempo por cada archivo que desee consultar, lo que puede dar la impresión que CFDInfo no está funcionando tan rápido como se espera.

Se recomienda que no active esta opción si desea consultar una cantidad inmensa de comprobantes fiscales, no solo por la cantidad de tiempo que CFDInfo se puede llevar en consultar cada comprobante, también porque desconozco la cantidad de consultas que puede soportar el servidor establecido por el SAT. Por favor, no abuse de esta característica.

### Desarrollo

Es bienvenido a contribuir al desarrollo de esta herramienta. Lo único que necesita instalar en su ordenador para comenzar es lo siguiente:

* Python 3.6.5 o mayor, con las siguientes dependencias:
	- pipenv

Una vez contando con lo anterior, clone el repositorio y ejecute el siguiente comando en el directorio creado:

`$ pipenv install`

Pipenv automáticamente procederá a crear un ambiente virtual con las dependencias del proyecto por usted. Si no está familiarizado con Pipenv, consulte la documentación aquí (sitio en inglés).