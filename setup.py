#!/usr/bin/env python3

'''NO FUNCIONA AÚN'''

from setuptools import setup, find_packages

with open ('README.md', 'rb') as f:
    long_descr = f.read()

setup(
    
    name = "CFDInfo",
    version = "0.1",
    packages = find_packages(),
    install_requires = [
        'lxml',
        'openpyxl',
        'suds-py3',
        ],

    author = "Israel Torres",
    author_email = "dnlsrl.kaiser@gmail.com",
    description = "Utilidad de línea de comandos que extrae información de un comprobante fiscal digital (CFDI)",

)