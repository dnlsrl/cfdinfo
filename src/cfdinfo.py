#!/usr/bin/env python3

'''
CREADO POR: Israel Torres
EMAIL: dnlsrl.kaiser@gmail.com
SITIO: dnlsrl.neocities.org


These violent delights have violent ends
'''

import argparse
from pathlib import Path
from datetime import date
from internals import sheetwriter
from collections import OrderedDict
from internals.xmlparser import getInfo
from internals.cfdivalidator import validate

def ynChoice(input_text, choices, default):
    if default is None:
        choice = input('{} [{}/{}] '.format(input_text, choices[0].lower(), choices[1].lower()))
        if choice.lower() == choices[0].lower():
            return True
        elif choice.lower() == choices[1].lower():
            return False
        else:
            print('Elija {} o {}'.format(choices[0].lower(), choices[1].lower()))
            ynChoice(input_text, choices, default)
    elif default is 1:
        choice = input('{} [{}/{}] '.format(input_text, choices[0].upper(), choices[1].lower()))
        if choice.lower() == choices[0].lower():
            return True
        elif choice.lower() == choices[1].lower():
            return False
        else:
            return True
    elif default is 0:
        choice = input('{} [{}/{}] '.format(input_text, choices[0].lower(), choices[1].upper()))
        if choice.lower() == choices[0].lower():
            return True
        elif choice.lower() == choices[1].lower():
            return False
        else:
            return False

def main():
    parser = argparse.ArgumentParser(prog = 'cfdinfo', description = 'Utilidad de línea de comandos que presenta datos de un XML en un XLSX')
    parser.add_argument('ruta', nargs = '*', help = '(Requerido) Introduzca una ruta a uno o varios archivos XML o a un directorio.')
    parser.add_argument('-v', '--validar', action = 'store_true', help = 'Use este argumento si desea que se valide la vigencia del comprobante fiscal.')
    parser.add_argument('-p', '--pantalla', action = 'store_true',
        help = 'Use este argumento si desea que los valores se muestren en la pantalla en lugar de crear un archivo.')
    args = parser.parse_args()

    if len(args.ruta) == 1:
        if Path(args.ruta[0]).exists() and Path(args.ruta[0]).is_file():
            values = getInfo(args.ruta[0])
            if args.validar:
                try:
                    values['Estado'] = validate(values['rfcEmisor'], values['rfcReceptor'], values['Total'],
                        values['UUID'])['Estado']
                except:
                    values['Estado'] = 'Error'
            if args.pantalla:
                toScreen(values, Path(args.ruta[0]), args.validar, len(args.ruta))
            else:
                if args.validar:
                    sheetwriter.singleFile(values, Path(args.ruta[0]), True)
                else:
                    sheetwriter.singleFile(values, Path(args.ruta[0]), False)
                try:
                    print('Se ha creado el archivo {}{}.xlsx'.format(values['Serie'], values['Folio']))
                except:
                    print('Se ha creado el archivo {}{}.xlsx'.format(values['serie'], values['folio']))
        elif Path(args.ruta[0]).exists() and Path(args.ruta[0]).is_dir():
            values = OrderedDict()
            paths = [x for x in list(Path(args.ruta[0]).glob('**/*.xml'))]
            if args.validar:
                print('El proceso de validación puede tomar algo de tiempo. {} comprobantes serán verificados'.format(len(paths)))
                choice = ynChoice('¿Desea continuar?', ['s', 'n'], 0)
            for x in paths:
                values['{}'.format(Path(x).name)] = getInfo(str(x))
                if args.validar and choice:
                    try:
                        values['{}'.format(Path(x).name)]['Estado'] = validate(values['{}'.format(Path(x).name)]['rfcEmisor'],
                            values['{}'.format(Path(x).name)]['rfcReceptor'], values['{}'.format(Path(x).name)]['Total'],
                            values['{}'.format(Path(x).name)]['UUID'])['Estado']
                    except:
                        values['{}'.format(Path(x).name)]['Estado'] = 'Error'
                else:
                    continue
            if args.pantalla:
                toScreen(values, paths, args.validar, len(paths))
            else:
                if args.validar:
                    sheetwriter.multipleFiles(values, paths, True)
                else:
                    sheetwriter.multipleFiles(values, paths, False)
                print('Se ha creado el archivo cfdinfo_{}.xlsx'.format(date.today().strftime('%d-%m-%Y')))
    elif len(args.ruta) > 1:
        values = OrderedDict()
        paths = []
        if args.validar:
            print('El proceso de validación puede tomar algo de tiempo. Múltiples comprobantes serán verificados')
            choice = ynChoice('¿Desea continuar?', ['s', 'n'], 0)
        for x in args.ruta:
            if Path(x).exists and Path(x).is_file():
                paths.append(Path(x))
                values['{}'.format(x)] = getInfo(x)
                if args.validar:
                    try:
                        values['{}'.format(x)]['Estado'] = validate(values['{}'.format(x)]['rfcEmisor'],
                            values['{}'.format(x)]['rfcReceptor'], values['{}'.format(x)]['Total'],
                            values['{}'.format(x)]['UUID'])['Estado']
                    except:
                        values['{}'.format(x)]['Estado'] = 'Error'
            elif Path(x).exists() and Path(x).is_dir():
                for y in Path(x).glob('**/*.xml'):
                    values['{}'.format(Path(y).name)] = getInfo(str(y))
                    paths.append(Path(y))
                    if args.validar and choice:
                        try:
                            values['{}'.format(Path(y).name)]['Estado'] = validate(values['{}'.format(Path(y).name)]['rfcEmisor'],
                                values['{}'.format(Path(y).name)]['rfcReceptor'], values['{}'.format(Path(y).name)]['Total'],
                                values['{}'.format(Path(y).name)]['UUID'])['Estado']
                        except:
                            values['{}'.format(Path(y).name)]['Estado'] = 'Error'
                    else:
                        continue
        if args.pantalla:
            toScreen(values, paths, args.validar, len(paths))
        else:
            if args.validar:
                sheetwriter.multipleFiles(values, paths, True)
            else:
                sheetwriter.multipleFiles(values, paths, False)
            print('Se ha creado el archivo cfdinfo_{}.xlsx'.format(date.today().strftime('%d-%m-%Y')))
    else:
        parser.print_help()

def toScreen(values, paths, validate, count):
    if count == 1:
        for a, b in values.items():
            print('{}: {}'.format(a, b))
        print('')
    else:
        for x in values:
            for a, b in values['{}'.format(x)].items():
                print('{}: {}'.format(a, b))
            print('')
    choice = ynChoice('¿Desea guardar estos datos en un archivo?', ['s', 'n'], None)
    if choice and count == 1:
        sheetwriter.singleFile(values, paths, validate)
        try:
            print('Se ha creado el archivo {}{}.xlsx'.format(values['Serie'], values['Folio']))
        except:
            print('Se ha creado el archivo {}{}.xlsx'.format(values['serie'], values['Folio']))
    elif choice and count < 1:
        sheetwriter.multipleFiles(values, paths, validate)
        print('Se ha creado el archivo cfdinfo_{}.xlsx'.format(date.today().strftime('%d-%m-%Y')))
    else:
        exit = input('Presione ENTER para terminar...')


if __name__ == '__main__':
    main()
