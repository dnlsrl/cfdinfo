#!/usr/bin/env python3

'''
ESCRIBE LOS VALORES DE LOS ARCHIVOS CFDI XML EN UNA HOJA DE CÁLCULO
'''

from pathlib import Path
from datetime import date
from openpyxl import Workbook
from collections import OrderedDict

def singleFile(values, path, validate):
    '''EXPORTA LOS DATOS DE UN SOLO ARCHIVO XML A UNA HOJA DE CÁLCULO (XLSX)'''

    wb = Workbook()
    ws = wb.active
    ws.title = 'CFDInfo'

    count = 0
    headers = [x for x in values]
    for row in ws.iter_rows(min_row = 1, max_col = len(headers), max_row = 1):
        for cell in row:
            cell.value = headers[count]
            count += 1

    count = 0
    col_count = 1
    for row in ws.iter_rows(min_row = 2, max_col = len(values), max_row = 2):
        for cell in row:
            if validate is True:
                if col_count == len(values) - 1:
                    cell.value = '=HIPERVINCULO("{}", "{}")'.format(str(path), values[headers[count]])
                else:
                    cell.value = values[headers[count]]
            else:
                if col_count == len(values):
                    cell.value = '=HIPERVINCULO("{}", "{}")'.format(str(path), values[headers[count]])
                else:
                    cell.value = cell.value = values[headers[count]]
            count += 1
            col_count += 1
        col_count = 1

    try:
        filename = '{}{}.xlsx'.format(values['Serie'], values['Folio'])
    except:
        filename = '{}{}.xlsx'.format(values['serie'], values['folio'])
    wb.save(filename)

def multipleFiles(values, paths, validate):
    '''EXPORTA LOS DATOS DE VARIOS ARCHIVOS XML A UNA HOJA DE CÁLCULO (XLSX)'''

    wb = Workbook()
    ws = wb.active
    ws.title = 'CFDInfo'

    count = 0
    headers = []
    for x in values:
        headers.append([n for n in values[x]])
    headers = max(headers, key=len)
    for row in ws.iter_rows(min_row = 1, max_col = len(headers), max_row = 1):
        for cell in row:
            cell.value = headers[count]
            count += 1

    count = 0
    col_count = 1
    file_count = 0
    xml_filenames = list(enumerate(values))
    xml_paths = list(enumerate(paths))
    for row in ws.iter_rows(min_row = 2, max_col = len(headers), max_row = len(values) + 1):
        for cell in row:
            try:
                if validate is True:
                    if col_count == len(headers) - 1:
                        cell.value = '=HIPERVINCULO("{}", "{}")'.format(str(xml_paths[file_count][1]),
                            values[xml_filenames[file_count][1]][headers[count]])
                    else:
                        cell.value = values[xml_filenames[file_count][1]][headers[count]]
                else:
                    if col_count == len(headers):
                        cell.value = '=HIPERVINCULO("{}", "{}")'.format(str(xml_paths[file_count][1]),
                            values[xml_filenames[file_count][1]][headers[count]])
                    else:
                        cell.value = values[xml_filenames[file_count][1]][headers[count]]
                count += 1
                col_count += 1
            except:
                count += 1
                col_count += 1
                continue
        file_count += 1
        col_count = 1
        count = 0

    filename = 'cfdinfo_{}.xlsx'.format(date.today().strftime('%d-%m-%Y'))
    wb.save(filename)