#!/usr/bin/env python3

'''
USA LOS DATOS DEL CFDI (RFC EMISOR, RFC RECEPTOR, TOTAL Y UUID) PARA VALIDAR SI EL COMPROBANTE ES AUTÉNTICO (?)
Créditos a
https://tar.mx/archivo/2016/validar-folio-fiscal-cfdi-con-php-directo-del-sat.html
http://pasted.co/edd5d3eb
'''

from suds.client import Client


def validate(rfcEmisor, rfcReceptor, total, UUID):
    client = Client('https://consultaqr.facturaelectronica.sat.gob.mx/ConsultaCFDIService.svc?wsdl')
    factura = '?re={}&rr={}&tt={}&id={}'.format(rfcEmisor, rfcReceptor, format(total, '017'), UUID)
    resultado = client.service.Consulta(factura)

    return resultado
