#!/usr/bin/env python3

'''
LEE LOS DATOS DE UN ARCHIVO XML Y LOS GUARDA EN UN DICCIONARIO ORDENADO (collections.OrderedDict())
'''

import argparse
from pathlib import Path
from lxml import etree as et
from collections import OrderedDict
try:
    from internals import cfdivalidator
except:
    import cfdivalidator


def getInfo(filename):

    valores = OrderedDict()
    version_cfdi = None
    buscar_comprobante = ['fecha', 'subtotal', 'descuento', 'tipocambio', 'moneda', 'total', 'tipodecomprobante', 'serie']
    impuestos = {'001': 'ISR', '002': 'IVA', '003': 'IEPS'}
    
    importe_count = 0
    impuesto_count = 0

    try:
        file = et.parse(filename)
        for elem in file.iter():
            if 'Comprobante' in elem.tag:
                for x in elem.attrib:
                    if x.lower() in buscar_comprobante:
                        try:
                            valores[x] = float(elem.attrib[x])
                        except:
                            valores[x] = elem.attrib[x]
                    elif x.lower() == 'folio':
                        valores[x] = elem.attrib[x]
                    elif x.lower() == 'version':
                        version_cfdi = float(elem.attrib[x])
            elif 'Emisor' in elem.tag:
                for x in elem.attrib:
                    if x.lower() == 'rfc':
                        valores['rfcEmisor'] = elem.attrib[x]
                    elif x.lower() == 'nombre':
                        valores['nombreEmisor'] = elem.attrib[x]
            elif 'Receptor' in elem.tag:
                for x in elem.attrib:
                    if x.lower() == 'rfc':
                        valores['rfcReceptor'] = elem.attrib[x]
                    elif x.lower() == 'nombre':
                        valores['nombreReceptor'] = elem.attrib[x]
            elif 'Impuestos' in elem.tag and 'Concepto' not in elem.getparent().tag:
                for x in elem.attrib:
                    try:
                        valores[x] = float(elem.attrib[x])
                    except:
                        valores[x] = elem.attrib[x]
                for x in elem.iterchildren():
                    for child in x.iterchildren():
                        if 'Traslado' in child.tag or 'Retencion' in child.tag:
                            if len(child.attrib) is not 0:
                                for x in child.attrib:
                                    if x.lower() == 'impuesto':
                                        if version_cfdi == 3.3:
                                            valores['impuesto_{}'.format(impuesto_count)] = impuestos[child.attrib[x]]
                                        else:
                                            valores['impuesto_{}'.format(impuesto_count)] = child.attrib[x]
                                        impuesto_count += 1
                                    elif x.lower() == 'importe':
                                        valores['importe_{}'.format(importe_count)] = float(child.attrib[x])
                                        importe_count += 1
            elif 'TimbreFiscalDigital' in elem.tag:
                for x in elem.attrib:
                    if x == 'UUID':
                        valores[x] = elem.attrib[x]
            else:
                continue
    except:
        print('El archivo {} no pudo ser procesado...'.format(Path(filename).name))
        valores['Err'] = 'Error en archivo'

    return valores

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('archivo', nargs = '*', help = 'Ruta a archivo(s) XML existente(s)')
    parser.add_argument('-v', '--validar', action = 'store_true', help = 'Use este argumento si desea validar la vigencia del comprobante fiscal')
    args = parser.parse_args()

    if len(args.archivo) == 1:
        if Path(args.archivo[0]).exists() and Path(args.archivo[0]).is_file():
            data = getInfo(args.archivo[0])
            if args.validar:
                try:
                    data['Estado'] = cfdivalidator.validate(data['rfcEmisor'], data['rfcReceptor'], data['Total'], data['UUID'])['Estado']
                except:
                    data['Estado'] = 'Error'
            for a, b in data.items():
                print('{}: {}'.format(a, b))
    else:
        data = OrderedDict()
        for x in args.archivo:
            if Path(x).exists() and Path(x).is_file():
                data['{}'.format(x)] = getInfo(x)
            if args.validar:
                try:
                    data['{}'.format(x)]['Estado'] = cfdivalidator.validate(data['{}'.format(x)]['rfcEmisor'], data['{}'.format(x)]['rfcReceptor'], 
                        data['{}'.format(x)]['Total'], data['{}'.format(x)]['UUID'])['Estado']
                except:
                    data['{}'.format(x)]['Estado'] = 'Error'
        for x in data:
            for a, b in data['{}'.format(x)].items():
                print('{}: {}'.format(a, b))
            print('')

if __name__ == '__main__':
    main()
